import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html'
})
export class ExamComponent implements OnInit {

  public find2Max2MinNumber(arr: Array<number>): void {
    arr.sort(compareNumeric);
    console.log('Исходные массив', arr, 'Второе наименьшие число массива =', arr[1], 'Второе наибольшие число массива =', arr[arr.length - 2]);
  }

  public differenceElements(arr1: Array<any>, arr2: Array<any>): void {
    let objectHelp = {};
    let arrayResult = [];
    let arr3 = arr1.concat(arr2);
    for (let i = 0; i < arr3.length; i++) {
      objectHelp[arr3[i]] = 1 + (objectHelp[arr3[i]] || 0);
    }
    for (let k in objectHelp) {
      arrayResult.push(k);
    }
    console.log('Исходные массивы =', arr1, 'и ', arr2);
    console.log('Объединение двух массивов и отсеивание повторяющихся дубликатов =', arrayResult);
  }

  public deleteDublicatsElements(arr: Array<any>): void {
    let objectHelp = {};
    let arrayResult = [];
    for (let i = 0; i < arr.length; i++) {
      objectHelp[arr[i]] = 1 + (objectHelp[arr[i]] || 0);
    }
    for (let k in objectHelp) {
      if (objectHelp[k] === 1) {
        arrayResult.push(k);
      }
    }
    console.log('Исходный массив =', arr);
    console.log('Массив с удаленными дублирующимися элеменами =', arrayResult);
  }

  public findLeapYear(a: number, b: number): void {
    let arrayYearsNumber = [];
    let arrayResults = [];
    for (let i = a - 1; i <= b - 1; i++) {
      arrayYearsNumber.push(i + 1);
    }
    for (let j = 0; j < arrayYearsNumber.length; j++) {
      if (arrayYearsNumber[j] % 4 === 0 && arrayYearsNumber[j] % 100 !== 0 || arrayYearsNumber[j] % 400 === 0) {
        arrayResults.push(arrayYearsNumber[j]);
      }
    }
    console.log('В заданном интревале лет', a, '-', b, 'високозными годами являются :', arrayResults)
  }

  public nFirstNumberFibonacci(n: number): void {
    n = n + 1;
    let fibonacci = [0, 1];
    for (let i = 2; i < n; i++) {
      fibonacci[i] = fibonacci[i - 1] + fibonacci[i - 2];
    }
    console.log('Получить первые n чисел Фибоначчи: ', fibonacci.slice(0, n));
  }

  public numberSystemConversions(value: any, from_base: number, to_base: number): void {
    value = this.changeSymbolsToLowerCase(value);
    let range = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ+/'.split('');
    let from_range = range.slice(0, from_base);
    let to_range = range.slice(0, to_base);
    let resultIn10 = value.split('').reverse().reduce(function (sumOneSymbol, symbol, power) {
      if (from_range.indexOf(symbol) === -1) throw new Error('Invalid symbol `' + symbol + '` for base ' + from_base + '.');
      return sumOneSymbol += from_range.indexOf(symbol) * (Math.pow(from_base, power));
    }, 0);
    let new_value = '';
    while (resultIn10 > 0) {
      new_value = to_range[resultIn10 % to_base] + new_value;
      resultIn10 = (resultIn10 - (resultIn10 % to_base)) / to_base;
    }
    console.log('Результат преобразования', value, 'из', from_base, 'системы счисления в', to_base, '=', new_value || '0');
  }

  public changeSymbolsToLowerCase(str: string): string {
    let LOWER = 'abcdefghijklmnopqrstuvwxyz';
    let result = [];
    for (let x = 0; x < str.length; x++) {
      if (LOWER.indexOf(str[x]) !== -1) {
        result.push(str[x].toUpperCase());
      } else {
        result.push(str[x]);
      }
    }
    return result.join('');
  }

  ngOnInit() {
    this.find2Max2MinNumber([1, 2, 3, 4, 5]);
    this.find2Max2MinNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 10]);
    this.find2Max2MinNumber([9, 0, 2, 10, 11, 6, 4, 29, 31]);
    this.find2Max2MinNumber([1090, 2233, 3343, 42, 52, 22, 1, 5466]);
    this.differenceElements([0, 10, 20], [20, 10, 30]);
    this.differenceElements([1, 2, 3], [100, 2, 1, 10]);
    this.differenceElements([5, 50, 41, 12], [12, 24, 9, 2, 1, 10]);
    this.differenceElements([10, 10, 2, 3], [10, 2, 1, 10]);
    this.differenceElements([1, 2, 3, 4, 5, 6, 7, 8], [1, 2, 3, 10, 4, 5]);
    this.deleteDublicatsElements([1, 2, 2, 4, 5, 4, 7, 8, 7, 3, 6]);
    this.deleteDublicatsElements([1, 2, 1, 2, 1, 2, 1, 2, 4, 3, 1]);
    this.deleteDublicatsElements([1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1]);
    this.deleteDublicatsElements([1068, 31, 21, 251, 11, 123, 6, 9, 26, 26, 14]);
    this.findLeapYear(2000, 2012);
    this.findLeapYear(1950, 2010);
    this.findLeapYear(2005, 2019);
    this.findLeapYear(100, 120);
    this.nFirstNumberFibonacci(8);
    this.nFirstNumberFibonacci(5);
    this.nFirstNumberFibonacci(3);
    this.nFirstNumberFibonacci(9);
    this.numberSystemConversions('E164', 16, 8);
    this.numberSystemConversions('1000', 2, 8);
    this.numberSystemConversions('128', 10, 2);
    this.numberSystemConversions('1353', 8, 10);
  }
}

function compareNumeric(a, b) {
  if (a > b) {
    return 1;
  }
  if (a < b) {
    return -1;
  }
}

function pow(x, n) {
  var result = x;
  for (var i = 1; i < n; i++) {
    result *= x;
  }
  return result;
}
