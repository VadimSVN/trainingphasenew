import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
  public arrayTask1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  public arrayConnect1 = [1, 2, 3, 4, 5];
  public arrayConnect2 = [6, 7, 8, 9, 10];
  public arrayBubble = [3, 6, 7, 9, 1, 2, 4, 5, 8, 10];
  public list = {
    value: 1,
    next: {
      value: 2,
      next: {
        value: 3,
        next: {
          value: 4,
          next: null
        }
      }
    }
  };
  public saveData: Array<number> = [];
  public arrayFind = ["test", 2, 1.5, false];
  public arrayFilterRange = [5, 4, 3, 8, 0];
  public arrayReverseNoNewArray = [6, 7, 3, 4, 5, 1, 2, 8, 9];
  public arrayCounterMaxElement = [3, 'a', 'a', 'a', 2, 3, 'a', 3, 'a', 2, 4, 9];
  public arraysFive = [[1, 2, 1, 24], [8, 11, 9, 4], [7, 0, 7, 27], [7, 4, 28, 14], [3, 10, 26, 7]];
  public arrayIntegers = [1, 2, 3, 4, 5, 6];

  public arraySeeWithoutReverse(arr: Array<number>): void {
    let x = [];
    for (let i = arr.length - 1; i >= 0; i--) {
      x.push(arr[i]);
    }
    console.log('>>> Развернуть массив в цикле(без метода) = ', x);
  }

  public arraySeeWithReverse(arr: Array<number>): void {
    console.log('>>> Развернуть массив в цикле(методом) = ', arr.reverse());
  }

  public arraySumWithoutConcat(arr1: Array<number>, arr2: Array<number>): void {
    let newArray = [];
    for (let i = 0; i < arr1.length; i++) {
      newArray.push(arr1[i]);
    }
    for (let i = 0; i < arr2.length; i++) {
      newArray.push(arr2[i]);
    }
    console.log('>>> Соеденить два массива(в цикле) = ', newArray);
  }

  public arraySumWithConcat(arr1: Array<number>, arr2: Array<number>): void {
    let newArray = arr1.concat(arr2);
    console.log('>>> Соеденить два массива(методом) = ', newArray);
  }

  public bubbleSort(arr: Array<number>): void {
    let n = arr.length;
    for (let i = 0; i < n - 1; i++) {
      for (let j = 0; j < n - 1 - i; j++) {
        if (arr[j + 1] < arr[j]) {
          let t = arr[j + 1];
          arr[j + 1] = arr[j];
          arr[j] = t
        }
      }
    }
    console.log('>>> Отсортировать массив пузырковой сортировкой = ', arr);
  }

  public printListRecursion(list): void {
    this.saveData.push(list.value);
    if (list.next) {
      this.printListRecursion(list.next);
    }
  }

  public find(arr, value): void {
    let x = -1;
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] === value) {
        x = i;
      }
    }
    console.log('>>> Результат поиска в массиве(номер элемента) = ', x);
  }

  public filterRange(arr, a, b): void {
    let x = [];
    for (let i = 0; i < arr.length; i++) {
      if (arr[i] >= a && arr[i] <= b) {
        x.push(arr[i]);
      }
    }
    x.join(',');
    console.log('>>> Фильтр диапазона чисел = ', x);
  }

  public reverseNoNewArray(arr: Array<any>): void {
    for (let j = 0; j < arr.length / 2; j++) {
      // в переменную записываем значение нашего 1-го элемента массива
      let t = arr[j];
      // последний элемент массива запиысваем в 1-й
      arr[j] = arr[arr.length - j - 1];
      // из переменной - запиысваем значение в последний элемент
      arr[arr.length - j - 1] = t;
      // и так - пока не пройдем весь массив
    }
    console.log('>>> Развернуть массив в цикле не создавая новый массив = ', arr);
  }

  public findMaxElement(arr: Array<any>): void {
    let objectHelp = {};
    let counterForElementResult;
    let elementResult;
    let arrayHelp = [];
    // пробегаемся по 1-му циклу и зщаписываем в Объект данные ключ:значение,
    // где ключ-элементы нашего базового массива, значение-количество раз, кот.данный элемент встречается
    for (let i = 0; i < arr.length; i++) {
      objectHelp[arr[i]] = 1 + (objectHelp[arr[i]] || 0);
    }
    // во 2-м цикле пробегаемся по значениям нашего заполненного Объекта и сортируем их,
    // чтобы наибольшие значения оказались вверху\внизу
    for (let k in objectHelp) {
      arrayHelp.push(objectHelp[k]);
      arrayHelp.sort(compareNumeric).reverse();
      counterForElementResult = arrayHelp[0];
    }
    // в 3-м цикле сравниваем значения нашего Объекта с получившимся максимальным значением и найдя -
    // выводим в когнсоль ключ данного значения
    for (let z in objectHelp) {
      if (objectHelp[z] === counterForElementResult) {
        elementResult = z;
        console.log('>>> Наиболее встречется элемент: ', elementResult, ', в количестве раз = ', counterForElementResult);
      }
    }
  }

  public factorial(num: number): number {
    if (num === 0) {
      return 1;
    }
    return num * this.factorial(num - 1);
  }

  public changeSymbols(str: string): void {
    let UPPER = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let LOWER = 'abcdefghijklmnopqrstuvwxyz';
    let result = [];
    for (let x = 0; x < str.length; x++) {
      if (UPPER.indexOf(str[x]) !== -1) {
        result.push(str[x].toLowerCase());
      }
      else if (LOWER.indexOf(str[x]) !== -1) {
        result.push(str[x].toUpperCase());
      }
      else {
        // other number, symbol, spaces
        result.push(str[x]);
      }
    }
    console.log('>>> Изменить в строке символы строчные/заглавные =', result.join(''));
  }

  public viewArraysSymbols(arr: Array<any>): void {
    let x = [];
    for (let i = 0; i < arr.length; i++) {
      console.log('строка "', i, '"')
      for (let j = 0; j < arr[i].length; j++) {
        console.log('"', arr[i][j], '"');
      }
    }
  }

  public arraySumProduct(arr: Array<number>): void {
    let x = 0;
    let y = 1;
    for (let i = 0; i < arr.length; i++) {
      x += arr[i];
    }
    console.log('>>> Сумма чисел массива =', x);
    for (let i = 0; i < arr.length; i++) {
      y *= arr[i];
    }
    console.log('>>> Произведение чисел массива =', y);
  }

  public isArray(item: any): boolean {
    let b = false;
    if (Array.isArray(item)) {
      b = true;
    }
    return b;
  }

  public сlone(arr: Array<any>): Array<any> {
    let x = [];
    if (arr.constructor === Array) {
      x = arr.slice();
      x.join(',');
    }
    return x;
  }

  public first(arr: Array<any>, value?: number): any {
    let a = [];
    if (value > 0) {
      // если длинна массива больше
      if (arr.length > value) {
        arr.length = value;
        for (let i = 0; i < arr.length; i++) {
          a.push(arr[i]);
        }
        // если длинна массива меньше или равно
      } else if (arr.length <= value) {
        for (let i = 0; i < arr.length; i++) {
          a.push(arr[i]);
        }
      }
    }
    else {
      a = arr[0];
    }
    if (value <= 0) {
      a = [];
    }
    return a;
  }

  public last(arr: Array<any>, value?: number): any {
    let a = [];
    if (value >= 0) {
      if (arr.length > value) {
        // вырезаем из массива элементы с arr.length - value и до конца
        a = arr.slice(arr.length - value);
      } else if (arr.length <= value) {
        for (let i = 0; i < arr.length; i++) {
          a.push(arr[i]);
        }
      }
    }
    else {
      a = arr.slice(arr.length - 1);
    }
    if (value <= 0) {
      a = [];
    }
    return a;
  }

  public myColor(arr: Array<any>, symbol: any): string {
    let str: string;
    str = arr.join(symbol);
    return str;
  }

  public difference(arr1: Array<any>, arr2: Array<any>): Array<any> {
    let objectHelp = {};
    let z = [];
    let arrayResult = [];
    let y: string;
    let x = arr1.concat(arr2);
    y = x.join(',');
    z = y.split(',');
    for (let i = 0; i < z.length; i++) {
      objectHelp[z[i]] = 1 + (objectHelp[z[i]] || 0);
    }
    for (let k in objectHelp) {
      if (objectHelp[k] === 1) {
        arrayResult.push(k);
      }
    }
    return arrayResult;
  }

  public isPrime(num): boolean {
    if (num < 2) return false;
    for (let i = 2; i < num; i++) {
      if (num % i == 0)
        return false;
    }
    return true;
  }

  ngOnInit() {
    // this.arraySeeWithoutReverse(this.arrayTask1);
    // this.arraySeeWithReverse(this.arrayTask1);
    // this.arraySumWithoutConcat(this.arrayConnect1, this.arrayConnect2);
    // this.arraySumWithConcat(this.arrayConnect1, this.arrayConnect2);
    // this.bubbleSort(this.arrayBubble);

     // this.printListRecursion(this.list);
     //  console.log('>>> Вывести односвязный список рекурсивно: ', this.saveData.join(", "));

    // this.find(this.arrayFind, 2);
    // this.filterRange(this.arrayFilterRange, 3, 5);
    // this.reverseNoNewArray(this.arrayReverseNoNewArray);
    // this.findMaxElement(this.arrayCounterMaxElement);
    // console.log('>>> Значение файториала 5! = ', this.factorial(5));
    // this.changeSymbols('The Quick Brown Fox--1112234-GGG');
    // this.viewArraysSymbols(this.arraysFive);
    // this.arraySumProduct(this.arrayIntegers);

    // console.log('Данный элемент является массивом? Ответ :', this.isArray('test'));
    // console.log('Данный элемент является массивом? Ответ :', this.isArray([1, 2, 4, 0]));
    // console.log('Клонированный массив выглядит так: ', this.сlone([1, 2, 4, 0]));
    // console.log('Клонированный массив выглядит так: ', this.сlone([1, 2, [4, 0]]));
    //
    // console.log('Получаем 1-й элемент массива = ', this.first([7, 9, 0, -2]));
    // console.log('Получаем 1-й элемент массива = ', this.first([], 3));
    // console.log('Получаем 1-й элемент массива = ', this.first([7, 9, 0, -2], 3));
    // console.log('Получаем 1-й элемент массива = ', this.first([7, 9, 0, -2], 6));
    // console.log('Получаем 1-й элемент массива = ', this.first([7, 9, 0, -2], -3));
    //
    // console.log('Получаем последний элемент массива = ', this.last([7, 9, 0, -2]));
    // console.log('Получаем последний элемент массива = ', this.last([7, 9, 0, -2], 3));
    // console.log('Получаем последний элемент массива = ', this.last([7, 9, 0, -2], 6));
    //
    // console.log('Соединяем массив в строку по знаку = ', this.myColor(["Red", "Green", "White", "Black"], ","));
    // console.log('Соединяем массив в строку по знаку = ', this.myColor(["Red", "Green", "White", "Black"], ";"));
    // console.log('Соединяем массив в строку по знаку = ', this.myColor(["Red", "Green", "White", "Black"], "+"));
    //
    // console.log('Выбираем уникальный элемент = ', this.difference([1, 2, 3], [100, 2, 1, 10]));
    // console.log('Выбираем уникальный элемент = ', this.difference([1, 2, 3, 4, 5], [1, [2], [3, [[4]]], [5, 6]]));

  }
}

function compareNumeric(a, b) {
  if (a > b) {
    return 1;
  }
  if (a < b) {
    return -1;
  }
}
