import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {ExamComponent} from "./exam/exam.component";
import {TestComponent} from "./test/test.component";

const routes: Routes = [
  {path: '', redirectTo: '/exam', pathMatch: 'full'},
  {path: 'exam', component: ExamComponent},
  {path: 'test', component: TestComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {useHash: true})],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
